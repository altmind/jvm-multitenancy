<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1380370668346" ID="ID_268450836" MODIFIED="1380370674012" TEXT="Multitenancy">
<node CREATED="1380370678257" ID="ID_767823124" MODIFIED="1380371751552" POSITION="right" TEXT="1. Issues">
<node CREATED="1380370680704" ID="ID_1789126831" MODIFIED="1380370684318" TEXT="Slow startup"/>
<node CREATED="1380370685944" ID="ID_1916362991" MODIFIED="1380370709201" TEXT="Memory consumption(COW?)"/>
</node>
<node CREATED="1380370819261" ID="ID_1705005519" MODIFIED="1380371129376" POSITION="left" TEXT="3. Multitenancy allow to host multiple apps in one JVM With respect to:">
<node CREATED="1380370843567" ID="ID_1641616089" MODIFIED="1380370852452" TEXT="Isolation"/>
<node CREATED="1380370858635" ID="ID_1104822639" MODIFIED="1380370867474" TEXT="Density(Less memory cosumption)"/>
</node>
<node CREATED="1380370894049" ID="ID_1690811774" MODIFIED="1380371131948" POSITION="right" TEXT="2. Possible solution - Multitenant JVM.">
<node CREATED="1380370939907" ID="ID_1187628551" MODIFIED="1380370940474" TEXT="Isolation due to isolated classloaders"/>
<node CREATED="1380370941378" ID="ID_1125507897" MODIFIED="1380371090580" TEXT="Density due to shared JITed code, shared static final vars, permgen"/>
<node CREATED="1380371157760" ID="ID_1224220974" MODIFIED="1380371178387" TEXT="Faster startup - hot cache and jit, already loaded SO"/>
</node>
<node CREATED="1380371253572" ID="ID_1069710816" MODIFIED="1380371261713" POSITION="left" TEXT="4, NO changes required">
<node CREATED="1380371262775" ID="ID_1565786350" MODIFIED="1380371269076" TEXT="just: java -Xmt -jar one.jar"/>
<node CREATED="1380371296953" ID="ID_1940518576" MODIFIED="1380371320943" TEXT="Yep, on first run JVM is started as daemon, next runs will use running daemon"/>
</node>
<node CREATED="1380371351385" ID="ID_1153266535" MODIFIED="1380371491963" POSITION="right" STYLE="fork" TEXT="7. Issues">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1380371355299" ID="ID_1302546285" MODIFIED="1380371561867" TEXT="No, Native libraries(JNI/JNA) are NOT isolated - including SWT"/>
<node CREATED="1380371516424" ID="ID_1980681733" MODIFIED="1380371537056" TEXT="Instrumentation calls(JVMTI - aka java debugger) - are not isolated"/>
</node>
<node CREATED="1380371399801" ID="ID_1932362103" MODIFIED="1380371404598" POSITION="left" TEXT="5. Resource limit">
<node CREATED="1380371409862" ID="ID_712449471" MODIFIED="1380371412946" TEXT="&#x2022; -Xlimit:cpu=10-30 (10 percent minimum CPU, 30 percent maximum)&#xa;&#x2022; -Xlimit:cpu=30 (30 percent maximum CPU)&#xa;&#x2022; -Xlimit:netIO=20M (maximum bandwidth of 20 Mbps)&#xa;&#x2022; -Xms8m-Xmx64m (initial 8 MB heap, 64 MB maximum)"/>
</node>
<node CREATED="1380371433583" ID="ID_365180595" MODIFIED="1380371440995" POSITION="right" TEXT="6. Benefits w/ numbers">
<node CREATED="1380371441949" ID="ID_1389733017" MODIFIED="1380371479369" TEXT="2 x higher number of tenants that classic JVM(jetty usecase)"/>
<node CREATED="1380371494353" ID="ID_1167720874" MODIFIED="1380371506933" TEXT="up to 6x lower startup time(jetty usecase)"/>
</node>
<node CREATED="1380371663846" ID="ID_1608869743" MODIFIED="1380371668746" POSITION="left" TEXT="9. See also : http://tinyurl.com/jvm-multitenancy"/>
</node>
</map>
